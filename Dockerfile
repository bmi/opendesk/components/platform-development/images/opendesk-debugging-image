# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-License-Identifier: Apache-2.0

FROM debian:bookworm@sha256:e97ee92bf1e11a2de654e9f3da827d8dce32b54e0490ac83bfc65c8706568116

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update \
 && apt-get -y install --no-install-recommends \
    nano \
    procps \
    curl \
    wget \
    ldap-utils \
    awscli \
    net-tools \
    lsof \
    netcat-openbsd \
    gnulib \
    jq \
    yq \
 && apt-get clean

LABEL org.opencontainers.image.authors="Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH" \
      org.opencontainers.image.documentation=https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-debugging-image/-/blob/main/README.md \
      org.opencontainers.image.source=https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-debugging-image \
      org.opencontainers.image.vendor="Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH" \
      org.opencontainers.image.licenses=Apache-2.0 \
      org.opencontainers.image.base.name=registry-1.docker.io/library/debian:bookworm \
      org.opencontainers.image.base.digest=sha256:e97ee92bf1e11a2de654e9f3da827d8dce32b54e0490ac83bfc65c8706568116

CMD ["/bin/bash"]
