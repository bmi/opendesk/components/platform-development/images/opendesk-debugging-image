<!--
# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-License-Identifier: Apache-2.0
-->

# openDesk Debugging container image

This image is for debugging purposes only. We use a debian base and `apt-get` install packages as needed.

Please check the [Dockerfile](./Dockerfile) for information on what packages get installed.
