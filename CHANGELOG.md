# [1.3.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-debugging-image/compare/v1.2.0...v1.3.0) (2024-07-24)


### Features

* **tools:** Add more tools: net-tools, lsof, netcat-openbsd, gnulib, jq, yq. ([710fd29](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-debugging-image/commit/710fd292a6c80e1efa53c6de2bddfd5075d4a9e8))

# [1.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-debugging-image/compare/v1.1.0...v1.2.0) (2024-07-04)


### Features

* **packages:** Install awscli. ([c99b24d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-debugging-image/commit/c99b24dbaaa19d0daec75cd68e0c9c56985ef6cf))

# [1.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-debugging-image/compare/v1.0.0...v1.1.0) (2024-07-01)


### Features

* **apt:** Install `ldaputils`. ([68b2e68](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-debugging-image/commit/68b2e687fe6bee3ddf18dac59e333bd82c6161f9))
* **apt:** Remove version pinning, as using the latest versions just makes sense when it comes to debugging. ([ada98c6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-debugging-image/commit/ada98c698f988cb3435fbc26096c3d1646d53178))

# 1.0.0 (2024-04-09)


### Features

* Initial commit ([0fbf908](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-debugging-image/commit/0fbf9088d8d7e14056c828107e34b95b54d8dd6e))
